# what-you-are-doing
## Projeto para controle de tarefas.

Aplicação para controle de tarefas, tendo como base uma separação simples das terefas.

Por ora a separação das tarefas será feita entre os tópicos, Desenvolvimento, Suporte e Implantação.

**Target inicial**
- Data prevista para o término da tarefa;
- Data que terminou a tarefa;
- Responsáveis (participantes da tarefa);

### Ferramentas para desenvolvimento:
- Apache TOMCAT 8.0.24 (Versão estável :: visto em 14/07/2015) [Download](http://tomcat.apache.org/download-80.cgi#8.0.24)
- Maven (Importar o projeto como "Projeto Maven")
- MySQL Workbench (O banco chama-se **wayd**) [Download](https://dev.mysql.com/downloads/workbench/)
 
### Configuração pré desenvolvimento:
Durante o desenvolvimento vimos que o 'estouro' de memória do java (PermGen) estava sendo constante. Levantei algumas informações e encontrei um parâmetro de inicialização da JVM que serve de solução paliativa:

**-XX:MaxPermSize=512m**

Esse parâmetro foi inserido no campo "Opções da VM" na janela de configação do servidor, via netbeans.
