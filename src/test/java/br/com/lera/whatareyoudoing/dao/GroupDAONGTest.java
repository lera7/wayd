/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.lera.whatareyoudoing.dao;

import br.com.lera.whatareyoudoing.jpa.LeraEntityManager;
import br.com.lera.whatareyoudoing.model.Group;
import java.util.List;
import javax.persistence.EntityManager;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author informix
 */
public class GroupDAONGTest {
    
    public GroupDAONGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        LeraEntityManager.setUp();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        LeraEntityManager.closeEntityManagerFactory();
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Teste de método getTasksList, da classe GroupDAO.
     */
//    @Test
//    public void testGetTasksList() {
//        System.out.println("getTasksList");
//        int groupId = 0;
//        GroupDAO instance = new GroupDAO();
//        List expResult = null;
//        List result = instance.getTasksList(groupId);
//        assertEquals(result, expResult);
//        // TODO verifica o código de teste gerado e remove a chamada default para falha.
//        fail("O caso de teste \u00e9 um prot\u00f3tipo.");
//    }

    /**
     * Teste de método getGroupList, da classe GroupDAO.
     */
    @Test
    public void testGetGroupList() {
        System.out.println("getGroupList");
        
        EntityManager entityManager = LeraEntityManager.createEntityManager();
        
        Group group = new Group("Grupo");
        Group group1 = new Group("Grupo1");
        Group group2 = new Group("Grupo2");
        Group group3 = new Group("Grupo3");
        
        GroupDAO instance = new GroupDAO(entityManager);
        
        instance.insert(group);
        instance.insert(group1);
        instance.insert(group2);
        instance.insert(group3);
        
        List result = instance.getGroupsList();
        
        Group resultGroup = (Group) result.get(0);
        Group resultGroup1 = (Group) result.get(1);
        Group resultGroup2= (Group) result.get(2);
        Group resultGroup3 = (Group) result.get(3);
        
        entityManager.close();
        
//        assertEquals(group.getGroupName(), resultGroup.getGroupName());
//        assertEquals(group1.getGroupName(), resultGroup1.getGroupName());
//        assertEquals(group2.getGroupName(), resultGroup2.getGroupName());
//        assertEquals(group3.getGroupName(), resultGroup3.getGroupName());
    }

//    /**
//     * Teste de método getGroupsWithTasks, da classe GroupDAO.
//     */
//    @Test
//    public void testGetGroupsWithTasks() {
//        System.out.println("getGroupsWithTasks");
//        GroupDAO instance = new GroupDAO();
//        List expResult = null;
//        List result = instance.getGroupsWithTasks();
//        assertEquals(result, expResult);
//        // TODO verifica o código de teste gerado e remove a chamada default para falha.
//        fail("O caso de teste \u00e9 um prot\u00f3tipo.");
//    }
    
}
