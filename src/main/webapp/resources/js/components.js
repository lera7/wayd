APP.components = APP.components ||

(function (doc, APP) {
    "use strict";
    
    var ENTER = 13;

    //MODELS
    function Group (properties) {
        var title = properties.groupName,
            tasks = properties.tasks,
            taskManager = properties.taskManager,
            groupId = properties.id,
            tasksArr = [], api, taskProperties, task,
            
            $addTask, $ul,
            $root = properties.root || $(doc.createElement("div")).attr("class", "section"),
            $title = $(doc.createElement("h3")).text(title),
            
            map = {
                $root : $root,
                $title : $title,
                $ul : $ul,
                tasks : tasksArr
            },
        
        _getJQueryMap = function () {
            return map;
        },
                
        _builder = function () {
            if (taskManager) {
                $ul = $(doc.createElement("ul")).attr("class", "task");
                for (var i in tasks) {
                    taskProperties = tasks[i];
                    taskProperties.groupId = groupId;

                    task = new Task(taskProperties);

                    tasksArr.push(task);

                    $ul.append(task.builder());
                }
            }
            
            return $root.append($title, $ul, $addTask);
        },
                
        _newTask = function () {
            var $addTaskButton = $(doc.createElement("a")).text("[+] Adicionar tarefa"),
                
                configInput = { 
                    options : ["Desenvolvimento", "Implantação", "Suporte"], 
                    close : true 
                },
                    
            _new = function (event) {
                var $self = $(this), 
                    $parent = $self.prev(),
                    iwdObj = inputWithDropdown(configInput),
                    $taskInput = iwdObj.getDOM(),
            
                _saveTask = function () {
                    Task.prototype.newTask({
                        groupid : groupId,
                        title : iwdObj.getValue().text,
                        description : iwdObj.getValue().combo
                    });
                };
                
                $taskInput.on("keypress", function (event) {
                    if (event.which === ENTER) {
                        _saveTask();
                        
                        iwdObj.destroy();
                    }
                });
                
                $parent.append($taskInput);
            };
            
            $addTaskButton.on("click", _new);
            
            return $addTaskButton;
        };
        
        if (taskManager) {
            $addTask = _newTask();
        }
        
        //API
        api = {
            builder : _builder,
            getJQueryMap : _getJQueryMap
        };
        
        return api;
    }
    
    Group.prototype.newGroup = function () {
        var $content = $(doc.createElement("div")),
            $groupName = $(doc.createElement("input")),
            $create = $(doc.createElement("a")),
            
        _saveGroup = function () {
            var name = $groupName.val(), ajaxOptions;
            if (name) {
                ajaxOptions = {
                    url : "group/create",
                    data : {
                        groupName : name
                    }
                };
                
                APP.ajaxcall(ajaxOptions);
            }
        };
    
        $groupName.attr("type", "text")
                .attr("name", "groupName");
        
        $create.text("[ Criar ]")
                .on("click", _saveGroup);
        
        $content.append($groupName, $create);
        
        return $content;
    };
    
    function Task (properties) {
        var title = properties.title,
            createDate = properties.createDate,
            groupId = properties.groupId,
            api,

//            calcDays = APP.dateDiff(createDate, new Date()),
            
            $root = $(doc.createElement("li")).attr("class", "task"),
            $input = $(doc.createElement("input")).attr("type", "checkbox"),
            $title =  $(doc.createElement("span")).attr("class", "task-name").text(title),
            $days = $(doc.createElement("span")).attr("class", "badge").text(createDate),

            map = {
                $root : $root,
                $input : $input,
                $title : $title,
                $days : $days
            },
                    
        _getJQueryMap = function () {
            return map;
        },

        _getGroupId = function () {
            return groupId;
        },
                
        _updateTitle = function (newTitle) {
            if (newTitle !== title) {
                title = newTitle;
                map.$title.text(title);
            }
        },
                
        _builder = function () {
            return $root.append($input, $title, $days);
        };

        api = {
            getGroupId : _getGroupId,
            updateTitle : _updateTitle,
            builder : _builder,
            getJQueryMap : _getJQueryMap
        };

        return api;
    }
    
    Task.prototype.newTask = function (taskData) {
        var ajaxOptions = {
                url : "task/create",
                data : taskData
            },
                    
        success = function (data) {
            console.log(data);
        };

        APP.ajaxcall(ajaxOptions, success);
    };
    
    //MANAGERS
    function createGroupList (config) {
        var $parent = config.$content,
            taskManager = !!config.taskManager,
            groupProperties, i, group,
            
            ajaxOptions = {
                url : "group/list"
            },
                    
        success = function (data) {
            var groups = data.data.groups;
            
            for (i in groups) {
                groupProperties = groups[i];
                groupProperties.taskManager = taskManager;
                group = new Group(groupProperties).builder();

                $parent.append(group);
            }
        };

        APP.ajaxcall(ajaxOptions, success);
    }
    
    function createTaskList (config) {
        var $parent = config.$content,
            groupProperties, i,
            groupArr = [], task,
            
            ajaxOptions = {
                url : "task/list"
            },
        
        success = function (data) {
            var tasks = data.data.tasks;
            
            console.log(tasks);
            
            for (i in tasks) {
                groupProperties = tasks[i];
                task = new Task(groupProperties);

                groupArr.push(task);
                $parent.append(task.builder());
            }
        };

        if (!$parent) {
            return false;
        }
    
        APP.ajaxcall(ajaxOptions, success);
    }
    
    //UTILS
    function inputWithDropdown (properties) {
        var options = properties.options, 
            
            //private datas
            api, comboValue, inputValue,
        
            //Elements
            $content = $(doc.createElement("div")),
            $inputGroup = $(doc.createElement("div")),
            $inputGroupBtn = $(doc.createElement("div")),
            $button = $(doc.createElement("button")),
            $ul = $(doc.createElement("ul")),
            $input = $(doc.createElement("input")),
            $caret = $('<span class="caret"></span>'),
            $closeInputGroup,
            $closeButton,
            
        _enableInput = function () {
            $input.prop("disabled", false);
        },
                
        _selected = function () {
            var $this = $(this);
            
            comboValue = $this.text();
            $button.text(comboValue + " ").append($caret);
            
            _enableInput();
        },
                
        _destroy = function () {
            //reset
            comboValue = inputValue = undefined;
            
            $content.remove(); 
        };
    
        //DOM manager
        $content.addClass("col-lg-12");
        
        $inputGroup.addClass("input-group");
        
        $inputGroupBtn.addClass("input-group-btn");
        
        $button.addClass("btn btn-default dropdown-toggle")
                .attr("type", "button")
                .attr("data-toggle", "dropdown")
                .text("Selecione...")
                .append($caret);
        
        $ul.addClass("dropdown-menu").attr("role", "menu");
        
        $input.addClass("form-control").prop("disabled", true);
        
        if (properties.close) {
            $closeInputGroup = $(doc.createElement("div")).addClass("input-group-btn");
            
            $closeButton = $(doc.createElement("button"))
                                .text("\xd7")
                                .addClass("btn btn-default dropdown-toggle")
                                .attr("type", "button")
                                .on("click", _destroy);
            
            $closeInputGroup.append($closeButton);
        }
        
        $content.append($inputGroup);
        $inputGroup.append($inputGroupBtn, $input, $closeInputGroup);
        $inputGroupBtn.append($button, $ul);
        
        for (var i in options) {
            $ul.append(
                    $(doc.createElement("li"))
                        .append(
                            $(doc.createElement("a"))
                                .text(options[i])
                                .on("click", _selected)
                        )
                    );
        }
        
        $content.getValue = function () {
            return $input.val();
        };
        
        //API
        api = {
            getValue : function () {
                inputValue = $input.val();
                
                return {
                    combo : comboValue,
                    text : inputValue
                };
            },
            
            getDOM : function () {
                return $content;
            },
            
            destroy : function () {
                _destroy();
            }
        };
        
        return api;
    }
    
    //EXPORT API
    return {
        createTaskList : function (config) {
            return createTaskList(config);
        },
        
        createGroupList : function (config) {
            return createGroupList(config);
        },
        
        createGroupForm : function () {
            return Group.prototype.newGroup();
        }
    };
    
})(document, APP);