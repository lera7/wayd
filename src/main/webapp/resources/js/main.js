var APP = 

(function () {
    var _MILLISECONDS_PER_SECONDS = 1000,
        _SECONDS_PER_HOUR = 3600,
        _HOURS_PER_DAY = 24,
        _DAYS_PER_MONTH = 30, //Avarege;
        _MONTHS_PER_YEAR = 12;
    
    return {
        
        /**
         * 
         * Diff between two dates
         * 
         * To 'method' param:
         * 1 - YEAR
         * 2 - MONTH
         * 3 - DAY
         * 
         * @param {String} date1
         * @param {String} date2
         * @param {Number} method
         * @returns {Number}
         */
        dateDiff : function (date1, date2, method) {
            var timeDiff,
                daysPerMonth = 1,
                monthsPerYear = 1,
                dateTime1 = (date1.constructor.name === "Date" ? date1 : new Date(date1.trim())).getTime(),
                dateTime2 = (date2.constructor.name === "Date" ? date2 : new Date(date2.trim())).getTime();
            
            if (!date1 || !date2) {
                console.log("Entradas inválidas");
                return;
            }
            
            var timeDiff = Math.abs(dateTime1 - dateTime2);
            
            if (!method) {
                return Math.floor(timeDiff / (_MILLISECONDS_PER_SECONDS * _SECONDS_PER_HOUR * _HOURS_PER_DAY));
            }
            
            switch (method) {
                case 1:
                    daysPerMonth = _DAYS_PER_MONTH;
                    monthsPerYear = _MONTHS_PER_YEAR;
                    break;
                    
                case 2:
                    daysPerMonth = _DAYS_PER_MONTH;
                    break;
            }
            
            return Math.floor(timeDiff / (_MILLISECONDS_PER_SECONDS * 
                                            _SECONDS_PER_HOUR *
                                            _HOURS_PER_DAY * 
                                            daysPerMonth * 
                                            monthsPerYear));
        },
        
        /**
         * Função auxiliar para chamadas com AJAX
         * @param {Object} options - Objeto de configuração do AJAX
         * @param {Function} callbackSuccess - Função de callback para sucesso
         * @param {Function} callbackError - Função de callback para erro
         * @param {Function} callbackAlways - Função de callback para qualquer resultado da operação
         * @returns {Boolean} - Retorna o resultado da operação. true para sucesso e false para erro
         */
        ajaxcall: function (options, callbackSuccess, callbackError, callbackAlways) {
            var defaults = {
                    type: "POST",
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    async: true
                },
                settings = (!options ? defaults : $.extend({}, defaults, options));

            if (!settings.url === "") {
                console.log("URL inválida");
                return false;
            }

            $.ajax(settings)
                    .done(function (data) {
                        console.log("SUCESSS CALL");
                        if ($.isEmptyObject(data)) {
                            console.log("EMPTY RESPONSE");
                            return false;
                        }

                        if (typeof callbackSuccess === "function") {
                            callbackSuccess(data);
                        }
                    })

                    .fail(function (jqXHR, textStatus, error) {
                        console.log("ERROR CALL:", error);
                        if (typeof callbackError === "function") {
                            callbackError(error);
                        }
                    })

                    .always(function (data_jqXHR, textStatus, jqXHR_errorThrown) {
                        if (typeof callbackAlways === "function") {
                            callbackAlways();
                        }
                    });
        }
    };
})();

(function init() {
    $(".load-page").on("click", function (event) {
        event.preventDefault();
        
        $("#main-content").load("to", { page : this.getAttribute("href") });
    });
})();