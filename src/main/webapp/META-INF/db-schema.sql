SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `wyad` ;
CREATE SCHEMA IF NOT EXISTS `wyad` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `wyad` ;

-- -----------------------------------------------------
-- Table `wyad`.`persons`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wyad`.`persons` ;

CREATE TABLE IF NOT EXISTS `wyad`.`persons` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `wyad`.`groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wyad`.`groups` ;

CREATE TABLE IF NOT EXISTS `wyad`.`groups` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(90) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wyad`.`tasks`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wyad`.`tasks` ;

CREATE TABLE IF NOT EXISTS `wyad`.`tasks` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) NOT NULL,
  `description` VARCHAR(120) NULL,
  `finish_date` DATE NOT NULL,
  `target_date` DATE NOT NULL,
  `groups_idgroups` INT NOT NULL,
  PRIMARY KEY (`id`, `groups_idgroups`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_tasks_groups1_idx` (`groups_idgroups` ASC),
  CONSTRAINT `fk_tasks_groups1`
    FOREIGN KEY (`groups_idgroups`)
    REFERENCES `wyad`.`groups` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `wyad`.`board`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wyad`.`board` ;

CREATE TABLE IF NOT EXISTS `wyad`.`board` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `wyad`.`board_tasks`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wyad`.`board_tasks` ;

CREATE TABLE IF NOT EXISTS `wyad`.`board_tasks` (
  `board_id` INT UNSIGNED NOT NULL,
  `tasks_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`board_id`, `tasks_id`),
  INDEX `fk_board_has_tasks_tasks1_idx` (`tasks_id` ASC),
  INDEX `fk_board_has_tasks_board_idx` (`board_id` ASC),
  CONSTRAINT `fk_board_has_tasks_board`
    FOREIGN KEY (`board_id`)
    REFERENCES `wyad`.`board` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_board_has_tasks_tasks1`
    FOREIGN KEY (`tasks_id`)
    REFERENCES `wyad`.`tasks` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `wyad`.`tasks_persons`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wyad`.`tasks_persons` ;

CREATE TABLE IF NOT EXISTS `wyad`.`tasks_persons` (
  `tasks_id` INT UNSIGNED NOT NULL,
  `persons_id` SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (`tasks_id`, `persons_id`),
  INDEX `fk_tasks_has_persons_persons1_idx` (`persons_id` ASC),
  INDEX `fk_tasks_has_persons_tasks1_idx` (`tasks_id` ASC),
  CONSTRAINT `fk_tasks_has_persons_tasks1`
    FOREIGN KEY (`tasks_id`)
    REFERENCES `wyad`.`tasks` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tasks_has_persons_persons1`
    FOREIGN KEY (`persons_id`)
    REFERENCES `wyad`.`persons` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
