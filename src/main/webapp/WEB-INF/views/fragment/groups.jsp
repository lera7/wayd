<%-- 
    Document   : group
    Created on : 14/06/2015, 00:10:11
    Author     : Rafael
--%>

<h1>Welcome to groups</h1>

<div id="group-content" class="col-lg-11 col-lg-offset-1"></div>

<script>
    var $GROUP_CONTENT = $(document.getElementById("group-content"));
            
    $GROUP_CONTENT.append(APP.components.createGroupForm());
    
    APP.components.createGroupList({
        $content : $GROUP_CONTENT,
        taskManager : true 
    });
</script>