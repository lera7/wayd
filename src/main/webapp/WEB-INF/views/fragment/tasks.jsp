<%-- 
    Document   : board
    Created on : 18/01/2015, 23:20:17
    Author     : Rafael
--%>

<h1>Welcome to tasks</h1>

<div id="task-content" class="col-lg-6 col-lg-offset-1"></div>

<script>
    var $TASK_CONTENT = $(document.getElementById("task-content"));
            
//    $TASK_CONTENT.append(APP.components.createGroupForm());
    
    APP.components.createTaskList({
        $content : $TASK_CONTENT,
        taskManager : true 
    });
</script>
