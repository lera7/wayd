
package br.com.lera.whatareyoudoing.interceptors;

import br.com.lera.whatareyoudoing.jpa.LeraEntityManager;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Leandro Manuel
 */
public class NavigateInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute("entityManager", LeraEntityManager.createEntityManager());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mav) throws Exception {
        EntityManager entityManager = (EntityManager) request.getAttribute("entityManager");
        if (entityManager.isOpen()) {
            entityManager.close();
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception excptn) throws Exception {
    }

}
