package br.com.lera.whatareyoudoing.controller;

import br.com.lera.whatareyoudoing.model.Test;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Rafael G. Francisco
 */
@org.springframework.stereotype.Controller
public class TestController {

    @RequestMapping("/get/test")
    public @ResponseBody Test test() {
        return new Test();
    }
    
}
