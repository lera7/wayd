package br.com.lera.whatareyoudoing.controller;

import br.com.lera.whatareyoudoing.dao.GroupDAO;
import br.com.lera.whatareyoudoing.jpa.LeraConnection;
import br.com.lera.whatareyoudoing.model.Group;
import br.com.lera.whatareyoudoing.model.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Rafael G. Francisco
 */
@org.springframework.stereotype.Controller
public class GroupController {

    @RequestMapping("/group/create")
    public @ResponseBody Response create(LeraConnection leraConnection, Group group) {
        new GroupDAO(leraConnection.getEntityManager()).insert(group);
        Response r = new Response();
        r.setStatus(true);
        
        return r;
    }
    
    @RequestMapping("/group/update")
    public @ResponseBody Response update(String newGroupName) {
        //update db here
        
        Response r = new Response();
        r.setStatus(true);
        
        return r;
    }
    
    @RequestMapping("/group/delete")
    public @ResponseBody Response delete(int groupId) {
        //verify if exists tasks in this group
        
        //delete tasks

        //delete db here
        
        Response r = new Response();
        r.setStatus(true);
        
        return r;
    }
    
    @RequestMapping("/group/list")
    public @ResponseBody Response getGroupsList(HttpServletRequest request) {
        List<Group> groupsList = 
                new GroupDAO((EntityManager) request.getAttribute("entityManager")).getGroupsWithTasks();
        Map data = new HashMap<>();
        data.put("groups", groupsList);
        
        Response r = new Response();
        r.setData(data);
        
        return r;
    }
    
}
