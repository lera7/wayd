package br.com.lera.whatareyoudoing.controller;

import br.com.lera.whatareyoudoing.dao.TaskDAO;
import br.com.lera.whatareyoudoing.model.Person;
import br.com.lera.whatareyoudoing.model.Response;
import br.com.lera.whatareyoudoing.model.Task;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Rafael G. Francisco
 */
@org.springframework.stereotype.Controller
public class TaskController {

    @RequestMapping("/task/create")
    public @ResponseBody Response create(HttpServletRequest request, Task task) {
        new TaskDAO((EntityManager) request.getAttribute("entityManager")).insert(task);
        Response r = new Response();
        r.setStatus(true);
        
        return r;
    }
    
    @RequestMapping("/task/update")
    public @ResponseBody Response update(HttpServletRequest request, Task task) {
        new TaskDAO((EntityManager) request.getAttribute("entityManager")).update(task);
        
        Response r = new Response();
        r.setStatus(true);
        
        return r;
    }
    
    @RequestMapping("/task/delete")
    public @ResponseBody Response delete(HttpServletRequest request, Task task) {
        new TaskDAO((EntityManager) request.getAttribute("entityManager")).remove(task);
        
        Response r = new Response();
        r.setStatus(true);
        
            return r;
    }
    
    @RequestMapping("/task/list")
    public @ResponseBody Response getGroupsList(HttpServletRequest request) {
        List<Task> tasksList = 
                new TaskDAO((EntityManager) request.getAttribute("entityManager")).getTasksList();
        Map data = new HashMap<>();
        data.put("tasks", tasksList);
//        data.put("tasks", new String[]{"ABC", "DEF"});
        
        Response r = new Response();
        r.setData(data);
        
        return r;
    }
    @RequestMapping("/task/list/{person}")
    public @ResponseBody Response getUserTasks(HttpServletRequest request, @RequestParam Person p) {
        EntityManager entity = (EntityManager) request.getAttribute("entityManager");
        
        List<Task> tasks = new TaskDAO(entity).getUserTasks(p);
        
        Map data = new HashMap<>();
        data.put("tasks", tasks);
        Response r = new Response();
        r.setStatus(true);
        r.setData(data);
        
        return r;
    }
    
}