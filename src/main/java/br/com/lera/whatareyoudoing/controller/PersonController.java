package br.com.lera.whatareyoudoing.controller;

import br.com.lera.whatareyoudoing.dao.PersonDAO;
import br.com.lera.whatareyoudoing.model.Person;
import br.com.lera.whatareyoudoing.model.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Leandro Manuel
 */
@org.springframework.stereotype.Controller
public class PersonController {
    
    @RequestMapping("/person/list")
    public Response list(HttpServletRequest request, Person p) {
        EntityManager entity = (EntityManager) request.getAttribute("entityManager");
        List<Person> persons = new PersonDAO(entity).list();
        
        Map data = new HashMap();
        data.put("persons", persons);
        
        Response r = new Response();
        r.setStatus(true);
        r.setData(data);
        
        return r;
    }
    
}
