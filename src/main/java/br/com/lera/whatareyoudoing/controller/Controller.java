package br.com.lera.whatareyoudoing.controller;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Rafael
 */
@org.springframework.stereotype.Controller
public class Controller {

    @RequestMapping("/olaMundoSpring")
    public String execute() {
        System.out.println("Executando a lógica com Spring MVC");
        return "ok";
    }

}
