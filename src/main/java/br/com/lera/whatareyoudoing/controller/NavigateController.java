package br.com.lera.whatareyoudoing.controller;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Rafael G. Francisco
 */
@org.springframework.stereotype.Controller
public class NavigateController {
    
    @RequestMapping("/to")
    public String page(String page) {
        return "fragment/" + page;
    }
    
}
