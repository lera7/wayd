
package br.com.lera.whatareyoudoing.listener;

import br.com.lera.whatareyoudoing.jpa.LeraEntityManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author Leandro Manuel
 */
@WebListener
public class ConnectionListener implements ServletContextListener {
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LeraEntityManager.setUp();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LeraEntityManager.closeEntityManagerFactory();
    }
    
}
