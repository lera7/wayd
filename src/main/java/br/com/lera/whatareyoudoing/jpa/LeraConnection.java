package br.com.lera.whatareyoudoing.jpa;

import javax.persistence.EntityManager;

/**
 * @author Rafael G. Francisco
 */
public class LeraConnection {

    EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public LeraConnection setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
        return this;
    }
    
}
