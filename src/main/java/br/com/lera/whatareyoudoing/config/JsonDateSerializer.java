package br.com.lera.whatareyoudoing.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Rafael G. Francisco
 */
public class JsonDateSerializer extends JsonSerializer<Calendar> {

    @Override
    public void serialize(Calendar calendar, JsonGenerator jg, SerializerProvider sp) throws IOException, JsonProcessingException {
        Date date = calendar.getTime();
        SimpleDateFormat ft = new SimpleDateFormat("YYYY--MM-dd");
        
        jg.writeString(ft.format(date));
    }

}
