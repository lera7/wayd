
package br.com.lera.whatareyoudoing.model;

import java.io.Serializable;
import java.util.HashMap;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Leandro Manuel
 */
@Entity
@Table(name = "boards")
public class Board implements Serializable {
    private static final long serialVersionUID = -2437478986123624748L;
    
    @Id
    @GeneratedValue
    private int id;
    private String title;
    private HashMap<Integer, Task> tasks;

    public Board(String title) {
        this.title = title;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public HashMap<Integer, Task> getTasks() {
        return tasks;
    }

    public void setTasks(HashMap<Integer, Task> tasks) {
        this.tasks = tasks;
    }
    
}
