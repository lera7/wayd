package br.com.lera.whatareyoudoing.model;

import br.com.lera.whatareyoudoing.config.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Leandro Manuel
 */
@Entity
@Table(name="tasks")
public class Task implements Serializable {
    private static final long serialVersionUID = 6481749203133375225L;

    @Id
    @GeneratedValue
    private Integer id;
    
    @Column(length = 150)
    @NotNull
    private String title;
    
    @Column(length = 320)
    private String description;
    
    @Temporal(TemporalType.DATE)
    private Calendar finishDate;
    
    @Temporal(TemporalType.DATE)
    private Calendar createDate = Calendar.getInstance();
    
    @ManyToMany(
        cascade = {CascadeType.ALL},
        fetch = FetchType.LAZY,
        mappedBy = "tasks",
        targetEntity = Person.class
    )
    private Set<Person> team;
    
    @JoinColumn(name="groupid")
    private Integer groupid;

    public Task() {
    }

//    public Task(String title, Set<Person> team, int groupid) {
//        this.title = title;
//        if (team == null || team.isEmpty()) {
//            this.team = new HashSet<>();
//        } else {
//            this.team = team;
//        }
//        this.groupid = groupid;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonSerialize(using=JsonDateSerializer.class)
    public Calendar getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Calendar finishDate) {
        this.finishDate = finishDate;
    }

    @JsonSerialize(using=JsonDateSerializer.class)
    public Calendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Calendar targetDate) {
        this.createDate = targetDate;
    }

//    public Set<Person> getTeam() {
//        return team;
//    }

//    public void setTeam(Set<Person> team) {
//        this.team = team;
//    }

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

}
