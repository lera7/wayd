package br.com.lera.whatareyoudoing.model;

import br.com.lera.whatareyoudoing.config.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Rafael G. Francisco
 */
public class Test {

    private String name = "classe";
    private int id = 300;
    private Calendar calendar = Calendar.getInstance();
    private Set<String> conj = new HashSet<>();

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @JsonSerialize(using=JsonDateSerializer.class) /* CLAP, CLAP, CLAP */
    public Calendar getCalendar() {
        return calendar;
    }
    
    public String getViewCalendar() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(calendar.getTime());
    }

    public Set<String> getConj() {
        conj.add("item1");
        conj.add("item2");
        conj.add("item3");
        return conj;
    }

}
