
package br.com.lera.whatareyoudoing.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Leandro Manuel
 */
@Entity
@Table(name = "groups")
public class Group implements Serializable {
    private static final long serialVersionUID = -510019173775306969L;
    
    @Id
    @GeneratedValue
    private int id;
    
    @NotNull
    String groupName;
    
    @OneToMany(mappedBy = "groupid")
    private Set<Task> tasks;

    public Group() {
        
    }
    
    public Group(String name) {
        this.groupName = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    
}
