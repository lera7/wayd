package br.com.lera.whatareyoudoing.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Leandro Manuel
 */
@Entity
@Table(name = "persons")
public class Person implements Serializable {
    private static final long serialVersionUID = -9008523483933062553L;

    @Id
    @GeneratedValue
    private int id;
    
    @NotNull
    private String name;
    
    @NotNull
    private String email;
    
    @ManyToMany(
        targetEntity = Task.class,
        cascade = {CascadeType.PERSIST, CascadeType.MERGE}
    )
    @JoinTable(
        name="persons_tasks",
        joinColumns=@JoinColumn(name="person_id"),
        inverseJoinColumns=@JoinColumn(name="task_id")
    )
    private Set<Task> tasks;

    public Person(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

}
