package br.com.lera.whatareyoudoing.model;

import java.util.Map;

/**
 * @author Rafael G. Francisco
 */
public class Response {
    
    private boolean status;
    private Map<String, Object> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
    
}
