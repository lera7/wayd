package br.com.lera.whatareyoudoing.dao;

import br.com.lera.whatareyoudoing.model.Person;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Leandro Manuel
 */
public class PersonDAO extends CUDManager{
    
    public PersonDAO(EntityManager entityManager) {
        super(entityManager);
    }
    
    public List<Person> list() {
        return entityManager.createQuery("SELECT p FROM Person p").getResultList();
    }
    
}
