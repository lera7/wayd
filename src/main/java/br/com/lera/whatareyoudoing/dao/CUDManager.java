package br.com.lera.whatareyoudoing.dao;

import javax.persistence.EntityManager;

/**
 *
 * @author Leandro Manuel
 */
public class CUDManager {

    protected EntityManager entityManager;

    public CUDManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void insert(Object entity) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(entity);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void remove(Object entity) {
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(entity);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public void update(Object entity) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(entity);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
