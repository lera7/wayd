
package br.com.lera.whatareyoudoing.dao;

import br.com.lera.whatareyoudoing.model.Group;
import br.com.lera.whatareyoudoing.model.Task;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Leandro Manuel
 */

public class GroupDAO extends CUDManager{

    public GroupDAO(EntityManager entityManager) {
        super(entityManager);
    }
    
    public List<Task> getTasksList(int groupId) {
        List resultList = entityManager.createQuery("SELECT t FROM Task t WHERE t.groupid = :groupId")
                .setParameter("groupId", groupId)
                .getResultList();
        
        return resultList;
    }
    
    public List<Group> getGroupsList() {
        List resultList = entityManager.createQuery("SELECT g FROM Group g")
                .getResultList();
        
        return resultList;
    }
    
    public List<Group> getGroupsWithTasks() {
        List resultList = entityManager.createQuery("SELECT g FROM Group g JOIN g.tasks")
                .getResultList();
        
        return resultList;
    }

}


