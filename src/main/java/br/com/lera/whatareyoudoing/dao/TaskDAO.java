package br.com.lera.whatareyoudoing.dao;

import br.com.lera.whatareyoudoing.model.Task;
import br.com.lera.whatareyoudoing.model.Person;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Leandro Manuel
 */
public class TaskDAO extends CUDManager {

    public TaskDAO(EntityManager entityManager) {
        super(entityManager);
    }

    public List<Task> getTasksList() {
        List tasks = entityManager.createQuery("SELECT t FROM Task t")
                .getResultList();
        
        return tasks;
    }
    
    public List<Task> getUserTasks(Person p) {
        List tasks = entityManager.createQuery("SELECT p.tasks FROM Person p WHERE p = " + p)
                .getResultList();
        
        return tasks;
    }

}
