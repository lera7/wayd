package br.com.lera.whatareyoudoing.system;

import br.com.lera.whatareyoudoing.model.Board;
import br.com.lera.whatareyoudoing.model.Task;
import java.util.Calendar;

/**
 *
 * @author Leandro Manuel
 */
public class TaskManager {

    /**
     * Defines the new Board for the Task (Moves a Task).
     *
     * @param oldBoard - The old Board.
     * @param newBoard - The new Board.
     * @param task - The Task to be moved;
     */
    public void setBoard(Board oldBoard, Task task, Board newBoard) {
        oldBoard.getTasks().remove(task.getId());
        newBoard.getTasks().put(task.getId(), task);
    }

    /**
     * Finishes a Task and returns whether a task was finished.
     * @param task - The Task to be finished.
     * @return true - If the Task was finished before its target date.
     */
    public void finish(Task task) {
        task.setFinishDate(Calendar.getInstance());
        
    }
}
